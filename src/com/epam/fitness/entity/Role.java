package com.epam.fitness.entity;

/**
 * Enum for user role.
 */
public enum Role {
    /**
     * Administrator.
     */
    ADMINISTRATOR("administrator"),
    /**
     * User.
     */
    USER("user"),
    /**
     * Trainer.
     */
    TRAINER("trainer");
    /**
     * Name.
     */
    private String name;

    /**
     * @param name name.
     */
    Role(final String name) {
        this.name = name;

    }
    /**
     *{@inheritDoc}
     */
    public Integer getIdentity() {
        return ordinal();
    }

    /**
     * @param identity id
     * @return role.
     */
    public static Role getByIdentity(final Integer identity) {
        return Role.values()[identity];
    }
}
