package com.epam.fitness.servlets;

import com.epam.fitness.action.*;
import com.epam.fitness.dao.pool.ConnectionPool;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Controller extends HttpServlet {
    private Map<String, Action> actionMap = new HashMap<String, Action>();

    public static final String DB_DRIVER_CLASS = "com.mysql.jdbc.Driver";
    public static final String DB_URL = "jdbc:mysql://localhost:3306/mydb?useSSL=false";
    public static final String DB_USER = "root";
    public static final String DB_PASSWORD = "myrootpass_5D";
    public static final int DB_POOL_START_SIZE = 20;
    public static final int DB_POOL_MAX_SIZE = 1000;
    public static final int DB_POOL_CHECK_CONNECTION_TIMEOUT = 0;

    public void init() {
        ConnectionPool.getInstance().init(DB_DRIVER_CLASS, DB_URL, DB_USER, DB_PASSWORD, DB_POOL_START_SIZE, DB_POOL_MAX_SIZE, DB_POOL_CHECK_CONNECTION_TIMEOUT);
        actionMap.put("login", new Authorization());
        actionMap.put("check_trainer", new CheckTrainer());
        actionMap.put("reduce_visit", new ReduceVisit());
        actionMap.put("unattach_trainer", new UnAttachTrainer());
        actionMap.put("accept_trainer", new AcceptTrainer());
        actionMap.put("logout", new LogOut());
        actionMap.put("make_order", new MakeOrder());
        actionMap.put("upBalance", new UpBalance());
        actionMap.put("users", new ShowUsers());
        actionMap.put("trainers", new ShowTrainers());
        actionMap.put("delete_user", new DeleteUser());
        actionMap.put("show_subscription", new ShowSubscription());
        actionMap.put("admin_back", new AdminBack());
        actionMap.put("user_subscription", new UserSubscription());
        actionMap.put("trainer_users", new ShowTrainerUsers());
        actionMap.put("training_info", new UpdateTrainingInfo());
        actionMap.put("aboutpage", new AboutPage());
        actionMap.put("contactpage", new ContactPage());
        actionMap.put("loginpage", new LoginPage());
        actionMap.put("homepage", new HomePage());
        actionMap.put("newspage", new NewsPage());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doGet(final HttpServletRequest req,
                      final HttpServletResponse resp)
            throws ServletException, IOException {
        this.process(req, resp);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doPost(final HttpServletRequest req,
                       final HttpServletResponse resp)
            throws ServletException, IOException {
        this.process(req, resp);
    }

    /**
     * @param req  request.
     * @param resp response.
     * @throws ServletException exception.
     * @throws IOException      exception.
     */
    private void process(final HttpServletRequest req,
                         final HttpServletResponse resp)
            throws ServletException, IOException {

        String actionKey = req.getParameter("action");
        Action action = actionMap.get(actionKey);
        String view = action.doAction(req, resp);
        if (view != null) {
            resp.sendRedirect(view);
        } else {
            req.getRequestDispatcher("/index.jsp");
        }
    }
}