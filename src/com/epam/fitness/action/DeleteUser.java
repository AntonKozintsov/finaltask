package com.epam.fitness.action;

import com.epam.fitness.dao.UserDao;
import com.epam.fitness.dao.mysql.UserDaoImpl;
import com.epam.fitness.entity.Role;
import com.epam.fitness.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Class to delete user.
 */
public class DeleteUser implements Action {
    /**
     * Log4j.
     */
    private static final Logger LOG =
            LogManager.getLogger(DeleteUser.class.getName());

    /**
     * {@inheritDoc}
     */
    @Override
    public String doAction(final HttpServletRequest req,
                           final HttpServletResponse resp) {
        String page;
        UserDao userDao = new UserDaoImpl();
        int id = Integer.parseInt(req.getParameter("user_id"));
        List<User> userList;
        User user;
        user = userDao.selectUser(id);
        userDao.deleteUser(id);
        userList = userDao.selectUser(Role.USER);
        req.removeAttribute("users");
        req.setAttribute("users", userList);
        page = "/users.jsp";
        LOG.info(user.toString() + " was successfully deleted");
        return page;
    }
}
