package com.epam.fitness.action;

import com.epam.fitness.dao.OrderDao;
import com.epam.fitness.dao.mysql.OrderDaoImpl;
import com.epam.fitness.entity.Order;
import com.epam.fitness.entity.Role;
import com.epam.fitness.entity.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class UserSubscription implements Action {
    @Override
    public String doAction(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        User user = (User) req.getSession().getAttribute("user");
        String page =null;
        int id = Integer.parseInt(req.getParameter("user_id"));
        System.out.println(id);
        OrderDao orderDao;
        List<Order> orderList;
        HttpSession session = req.getSession(false);
        orderDao = new OrderDaoImpl();
        orderList = orderDao.selectOrder(id);
        session.setAttribute("orders", orderList);
        if (user.getRole().equals(Role.ADMINISTRATOR)) {
            page="/users.jsp";
        } else {
            page="/trainer.jsp";
        }
        return page;
    }
}
