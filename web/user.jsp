<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <%--<link rel="stylesheet" type="text/css" href="website/css/style.css">--%>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/website/css/style.css"/>
    <!-- Bootstrap CSS -->
    <script type="text/javascript">
        <%@include file="/website/js/scripts.js"%>
    </script>
    <script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>
    <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js'></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Fitness</title>
</head>
<body>
<div class="userInf">
    <c:set var="user" value="${user}"/>
    <div class="text userName"><c:out value="${user.getName()}"/></div>
    <div class="text money"><c:out value="${user.getMoney()}$"/></div>
    <form method="post" action="fitness">
        <button name="action" value="logout" class="btn btn-primary logout">LOGOUT</button>
    </form>
    <form method="post" action="fitness">
        <button name="action" value="show_subscription" class="btn btn-primary logout">SHOW SUBSCRIPTIONS</button>
        <input type="hidden" name="user_id" value="${user.getIdentity()}">
    </form>

    <button type="button" class="btn btn-primary logout" data-toggle="modal" data-target="#exampleModal"
            data-whatever="@mdo">UP BALANCE
    </button>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">UP BALANCE</h4>
                </div>
                <form method="post" action="fitness">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="upBalance" class="form-control-label">Money:</label>
                            <input type="number" name="money" class="form-control" id="upBalance" min="1" max="100">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" name="action" value="upBalance" class="btn btn-primary">Ok</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="conteiner-fluid sectin2">
    <div class="row">
        <div class="col-md-12 text-center heading-wrap userMargin">
            <h2>Featured Classes</h2>
            <span class="back-text">The Classes</span>
        </div>
    </div>
</div>
<form method="post" action="fitness">
    <div class="conteiner-fluid">
        <div class="conteiner3">
            <div class="row text-center">
                <div class="col-4 zoom"><img src="website/img/img_1_square.jpg" alt="#" class="w-100">
                    <div class="text img1">
                        <button id="ord1" type="submit" name="action" value="make_order" class="price"
                                onclick="submitForm(this)">
                            <c:if test="${user.getTotalSum()>=200 && user.getTotalSum()<500}">
                                <c:out value="${20-3}"/>
                            </c:if>
                            <c:if test="${user.getTotalSum()>=500}">
                                <c:out value="${20-5}"/>
                            </c:if>
                            <c:if test="${user.getTotalSum()<200}">
                                <c:out value="${20}"/>
                            </c:if>
                        </button>
                        <p class="textbox">8 VISITS</p>
                    </div>
                </div>
                <div class="col-4 zoom"><img src="website/img/img_2_square.jpg" alt="#" class="w-100">
                    <div class="text img1">
                        <button id="ord2" name="action" value="make_order" class="price" onclick="submitForm(this)">
                            <c:if test="${user.getTotalSum()>=200 && user.getTotalSum()<500}">
                                <c:out value="${25-3}"/>
                            </c:if>
                            <c:if test="${user.getTotalSum()>=500}">
                                <c:out value="${25-5}"/>
                            </c:if>
                            <c:if test="${user.getTotalSum()<200}">
                                <c:out value="${25}"/>
                            </c:if>
                        </button>
                        <p class="textbox">12 VISITS</p>
                    </div>
                </div>
                <div class="col-4 zoom"><img src="website/img/img_3_square.jpg" alt="#" class="w-100">
                    <div class="text img1">
                        <button id="ord3" name="action" value="make_order" class="price" onclick="submitForm(this)">
                            <c:if test="${user.getTotalSum()>=200 && user.getTotalSum()<500}">
                                <c:out value="${30-3}"/>
                            </c:if>
                            <c:if test="${user.getTotalSum()>=500}">
                                <c:out value="${30-5}"/>
                            </c:if>
                            <c:if test="${user.getTotalSum()<200}">
                                <c:out value="${30}"/>
                            </c:if>
                        </button>
                        <p class="textbox">15 VISITS</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type='hidden' id='cost' name='cost'>
</form>
<c:if test="${sessionScope.orders != null}">
    <div class="conteiner-fluid sectin2">
        <div class="row">
            <div class="col-md-12 text-center heading-wrap">
                <h2>Your subscriptions</h2>
            </div>
        </div>
    </div>
    <div class="midtable">
        <table>
            <thead>
            <tr>
                <th><c:out value="cost"/></th>
                <th><c:out value="date"/></th>
                <th><c:out value="expiration"/></th>
                <th><c:out value="visits"/></th>
                <th><c:out value="trainer"/></th>
                <th><c:out value="Training info"/></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="order" items="${sessionScope.orders}">
                <tr>
                    <td><c:out value="${order.getCost()}"/></td>
                    <td><c:out value="${order.getDate()}"/></td>
                    <td><c:out value="${order.getExpiration()}"/></td>
                    <td><c:out value="${order.getSubscription().getVisitCount()}"/></td>
                    <c:if test="${order.getSubscription().getTrainer().getIdentity()==null}">
                    <td>
                        <form action="fitness" method="post">
                            <input type="hidden" name="subscription_id"
                                   value="${order.getSubscription().getIdentity()}">
                            <button type="submit" name="action" value="accept_trainer" class="btn btn-info"><c:out
                                    value="Accept Trainer"/></button>
                        </form>
                    </td>
                    </c:if>

                    <c:if test="${order.getSubscription().getTrainer().getIdentity()!=null}">
                        <td><c:out value="${order.getSubscription().getTrainer().getName()}"/></td>
                    </c:if>
                    <c:if test="${order.getSubscription().getInfo()==null}">
                        <td><c:out value="${'No training info yet'}"/></td>
                    </c:if>
                    <c:if test="${order.getSubscription().getInfo()!=null}">
                        <td><c:out value="${order.getSubscription().getInfo()}"/></td>
                    </c:if>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>
    ${sessionScope.orders=null}
</c:if>
<div class="conteiner-fluid sectin2">
    <div class="row">
        <div class="col-md-12 text-center heading-wrap">
            <h2>Expert treiners</h2>
            <span class="back-text">Our Trainers</span>
        </div>
    </div>
</div>
<form method="post" action="fitness">
    <div class="conteiner-fluid">
        <div class="row text-center">
            <div class="col-sm-1"></div>

            <div class="col treiners">
                <button id="trainer1" type="submit" name="action" value="check_trainer" class="unstyled-button" onclick="submitForm(this)"><img
                        src="website/img/person_1.jpg" alt="#" class="w-100 sqr9"></button>
            </div>

            <div class="col treiners">
                <button id="trainer2" type="submit" name="action" value="check_trainer" class="unstyled-button" onclick="submitForm(this)"><img
                        src="website/img/person_2.jpg" alt="#" class="w-100 sqr9"></button>
            </div>

            <div class="col treiners">
                <button id="trainer3" type="submit" name="action" value="check_trainer" class="unstyled-button" onclick="submitForm(this)"><img
                        src="website/img/person_3.jpg" alt="#" class="w-100 sqr9"></button>
            </div>
            <div class="col-sm-1"></div>
        </div>
    </div>
    <input type="hidden" name="user_id" value="${user.getIdentity()}">
    <input type='hidden' id='trainer_id' name='trainer_id'>
</form>


<div class="conteiner-fluid">
    <div class="row text-center">
        <div class="col-sm-1"></div>
        <div class="col">
            <div class="text">
                <h2>Mellisa Howard</h2>
                <p class="text NameTr">Gym Trainer</p>
            </div>
        </div>
        <div class="col">
            <div class="text">
                <h2>Mike Richardson</h2>
                <p class="text NameTr">Gym Trainer</p>
            </div>
        </div>
        <div class="col">
            <div class="text">
                <h2>Charles White</h2>
                <p class="text NameTr">Gym Trainer</p>
            </div>
        </div>
        <div class="col-sm-1"></div>
    </div>
</div>
<c:if test="${sessionScope.message != null}">
<script>
var alertMessage = "${sessionScope.message}";
actionAlert(alertMessage);
${sessionScope.message=null}
</script>
</c:if>
<footer class="page-footer">
    <div class="container-fluid text-center text-md-left">
        <div class="row">
            <div class="col-4"></div>
            <div class="col-2">
                <h3 class="text-white">About Us</h3>
                <p class="grey">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus et dolor
                    blanditiis consequuntur ex voluptates perspiciatis omnis unde minima expedita.</p>
            </div>
            <div class="col-2">
                <h3 class="text-white">Contact Info</h3>
                <ul class="list-unstyled">
                    <li class="d-block">
                        <span class="d-block grey">Address:</span>
                        <span class="text-white">34 Street Name, City Name Here, United States</span></li>
                    <li class="d-block"><span class="d-block grey">Telephone:</span><span class="text-white">+1 242 4942 290</span>
                    </li>
                    <li class="d-block"><span class="d-block grey">Email:</span><span class="text-white">info@yourdomain.com</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
</body>
</html>