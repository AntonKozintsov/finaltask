package com.epam.fitness.action;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CheckTrainer implements Action {
    @Override
    public String doAction(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String page;
        String trainer_id = req.getParameter("trainer_id");
        req.getSession().setAttribute("trainer_id", trainer_id);
        Action action = new ShowSubscription();
        page = action.doAction(req,resp);
        return page;
    }
}
