package com.epam.fitness.dao.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

final public class ConnectionPool {

	private String url;
	private String user;
	private String password;
	private int maxSize;
	private int checkConnectionTimeout;

	private BlockingQueue<Connection> freeConnections = new LinkedBlockingQueue<>();
	private BlockingQueue<Connection> usedConnections = new LinkedBlockingQueue<>();

	private ConnectionPool() {}

	public synchronized Connection getConnection()  {
		Connection connection = null;
		while(connection == null) {
			try {
				if(!freeConnections.isEmpty()) {
					connection = freeConnections.take();
					if(!connection.isValid(checkConnectionTimeout)) {
						freeConnection(connection);
						connection = null;
					}
				} else if(usedConnections.size() < maxSize) {
					connection = createConnection();
				} else {
//					logger.error("The limit of number of database connections is exceeded");
//					throw new PersistentException();
				}
			} catch(InterruptedException | SQLException e) {
//				logger.error("It is impossible to connect to a database", e);
//				throw new PersistentException(e);
			}
		}
		usedConnections.add(connection);
//		logger.debug(String.format("Connection was received from pool. Current pool size: %d used connections; %d free connection", usedConnections.size(), freeConnections.size()));
		return connection;
	}

	public synchronized void freeConnection(Connection connection) {
		try {
			if(connection.isValid(checkConnectionTimeout)) {
				connection.clearWarnings();
				connection.setAutoCommit(true);
				usedConnections.remove(connection);
				freeConnections.put(connection);
//				logger.debug(String.format("Connection was returned into pool. Current pool size: %d used connections; %d free connection", usedConnections.size(), freeConnections.size()));
			}
		} catch(SQLException | InterruptedException e1) {
//			logger.warn("It is impossible to return database connection into pool", e1);
			try {
				connection.close();
			} catch(SQLException e2) {}
		}
	}

	public synchronized void init(String driverClass, String url, String user, String password, int startSize, int maxSize, int checkConnectionTimeout) {
		try {
			destroy();
			Class.forName(driverClass);
			this.url = url;
			this.user = user;
			this.password = password;
			this.maxSize = maxSize;
			this.checkConnectionTimeout = checkConnectionTimeout;
			for(int counter = 0; counter < startSize; counter++) {
				freeConnections.put(createConnection());
			}
		} catch(ClassNotFoundException | SQLException | InterruptedException e) {
//			logger.fatal("It is impossible to initialize connection pool", e);
//			throw new PersistentException(e);
		}
	}

	private static ConnectionPool instance = new ConnectionPool();

	public static ConnectionPool getInstance() {
		return instance;
	}

	private Connection createConnection() throws SQLException {
		return DriverManager.getConnection(url, user, password);
	}

	public synchronized void destroy() {
		usedConnections.addAll(freeConnections);
		freeConnections.clear();
		for(Connection connection : usedConnections) {
			try {
				connection.close();
			} catch(SQLException e) {}
		}
		usedConnections.clear();
	}
}
