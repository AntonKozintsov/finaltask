package com.epam.fitness.dao.mysql;

import com.epam.fitness.dao.SubscriptionDao;
import com.epam.fitness.dao.pool.ConnectionPool;
import com.epam.fitness.entity.Subscription;
import com.epam.fitness.entity.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class SubscriptionDaoImpl extends BaseDaoImpl implements SubscriptionDao {

    @Override
    public List<Subscription> selectSubscriptionList(int id) {
        return null;
    }

    @Override
    public void insert(Subscription subscription) {
        String sql = "INSERT INTO `subscription` "
                + "(`user_id`,`visit_count`, `trainer_id`, `info`) VALUES(?, ?, ?, ?)";
        PreparedStatement statement = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, subscription.getUser().getIdentity());
            statement.setInt(2, subscription.getVisitCount());
            if (subscription.getTrainer().getIdentity() == null) {
                statement.setNull(3, java.sql.Types.INTEGER);
            } else {
                statement.setInt(3, subscription.getTrainer().getIdentity());
            }
            statement.setString(4, subscription.getInfo());
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            super.closeConnection(connection);
            super.closeStatement(statement);
        }
    }

    @Override
    public Subscription selectSubscriptionDesc(int userId) {
        String sql = "SELECT * FROM `subscription` u WHERE u.`user_id` = ? order by id desc limit 1";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Subscription subscription = new Subscription();
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, userId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                subscription = create(resultSet);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            super.closeConnection(connection);
            super.closeStatement(preparedStatement);
        }
        return subscription;
    }

    @Override
    public void updateTrainerInfo(int subscription_id, int trainer_id) {
        String sql = "UPDATE `subscription` u SET u.`trainer_id` = ?, u.`info` = ? WHERE u.`id` = ?";
        UserDaoImpl userDao = new UserDaoImpl();
        User trainer = userDao.selectUser(trainer_id);
        String trainingInfo = "No training info yet";
        switch (trainer.getName()) {
            case "Melisa":
                trainingInfo = "Hyper intensive training and weight loss programm";
                break;
            case "Mike":
                trainingInfo = "Weight gain programm";
                break;
            case "Charles":
                trainingInfo = "Yoga and stretching";
                break;
        }
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, trainer_id);
            preparedStatement.setString(2, trainingInfo);
            preparedStatement.setInt(3, subscription_id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            super.closeConnection(connection);
            super.closeStatement(preparedStatement);
        }
    }

    @Override
    public Subscription selectSubscription(int subscriptionId) {
        String sql = "SELECT * FROM `subscription` u WHERE u.`id` = ?";
        PreparedStatement preparedStatement = null;
        final ResultSet resultSet;
        Subscription subscription = new Subscription();
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, subscriptionId);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                subscription = create(resultSet);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            super.closeConnection(connection);
            super.closeStatement(preparedStatement);
        }
        return subscription;
    }

    private Subscription create(ResultSet resultSet) {
        Subscription subscription = new Subscription();
        UserDaoImpl userDao = new UserDaoImpl();
        if (resultSet != null) {
            try {
                subscription.setIdentity(resultSet.getInt("id"));
                subscription.setUser(userDao.selectUser(resultSet.getInt("user_id")));
                subscription.setTrainer(userDao.selectUser(resultSet.getInt("trainer_id")));
                subscription.setVisitCount(resultSet.getInt("visit_count"));
                subscription.setInfo(resultSet.getString("info"));
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return subscription;
    }

    public void updateVisit(int visit_count, int subscription_id) {
        String sql = "UPDATE `subscription` u SET u.`visit_count` "
                + "= ? WHERE u.`id` = ?";
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, visit_count);
            preparedStatement.setInt(2, selectSubscription(subscription_id).getIdentity());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            super.closeConnection(connection);
            super.closeStatement(preparedStatement);
        }
    }
    public void updateInfo(String info, int subscription_id) {
        String sql = "UPDATE `subscription` u SET u.`info` "
                + "= ? WHERE u.`id` = ?";
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, info);
            preparedStatement.setInt(2, selectSubscription(subscription_id).getIdentity());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            super.closeConnection(connection);
            super.closeStatement(preparedStatement);
        }
    }
    public void updateTrainer(int subscription_id) {
        String sql = "UPDATE `subscription` u SET u.`trainer_id` = ? WHERE u.`id` = ?";
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setNull(1, java.sql.Types.INTEGER);
            preparedStatement.setInt(2, selectSubscription(subscription_id).getIdentity());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            super.closeConnection(connection);
            super.closeStatement(preparedStatement);
        }
    }
}
