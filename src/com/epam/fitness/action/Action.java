package com.epam.fitness.action;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Interface for action classes.
 */
public interface Action {
    /**
     * Method to perform action.
     * @param req request
     * @param resp response
     * @throws IOException exception
     * @throws ServletException exception
     */
    String doAction(HttpServletRequest req, HttpServletResponse resp)
            throws IOException, ServletException;

}
