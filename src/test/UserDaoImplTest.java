package test;

import com.epam.fitness.dao.UserDao;
import com.epam.fitness.dao.mysql.UserDaoImpl;
import com.epam.fitness.dao.pool.ConnectionPool;
import com.epam.fitness.util.HashPassword;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.sql.SQLException;

/**
 * Class to test user dao.
 */
public class UserDaoImplTest {
    public static final String DB_DRIVER_CLASS = "com.mysql.jdbc.Driver";
    public static final String DB_URL = "jdbc:mysql://localhost:3306/mydb?useSSL=false";
    public static final String DB_USER = "root";
    public static final String DB_PASSWORD = "myrootpass_5D";
    public static final int DB_POOL_START_SIZE = 10;
    public static final int DB_POOL_MAX_SIZE = 1000;
    public static final int DB_POOL_CHECK_CONNECTION_TIMEOUT = 0;

    @BeforeTest
    public void doBeforeTest() {

        ConnectionPool.getInstance().init(DB_DRIVER_CLASS, DB_URL, DB_USER, DB_PASSWORD, DB_POOL_START_SIZE, DB_POOL_MAX_SIZE, DB_POOL_CHECK_CONNECTION_TIMEOUT);
    }

    /**
     * @return data to test.
     */
    @DataProvider(name = "isExist")
    public Object[][] isExist() {
        String email = "pavel@gmail.com";
        String passWord = "123456";
        passWord = HashPassword.generateHashMD5(passWord);

        return new Object[][]{
                {email, passWord, true}};
    }

    /**
     * @return data to test.
     */
    @DataProvider(name = "updateMoney")
    public Object[][] updateMoney() {
        String email = "pavel@gmail.com";
        double money = 50.0;
        return new Object[][]{
                {email, money}};
    }

    /**
     * @param email user email.
     * @param money user money.
     */
    @Test(dataProvider = "updateMoney")
    public void testUpdateMoney(final String email, final double money) {
        UserDao userDao = new UserDaoImpl();
        userDao.updateMoney(email, money);
        Assert.assertEquals(money, userDao.selectUser(email).getMoney());
    }
    public UserDaoImplTest() throws SQLException {
    }
}
