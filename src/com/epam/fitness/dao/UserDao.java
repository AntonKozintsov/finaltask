package com.epam.fitness.dao;

import com.epam.fitness.entity.Role;
import com.epam.fitness.entity.User;

import java.util.List;

/**
 * Class for user dao.
 */
public interface UserDao {
    /**
     * @param email user Email
     * @return user.
     */
    User selectUser(String email);
    /**
     * @param email user email.
     * @param money user deposit money.
     */
    void updateMoney(String email, Double money);
    /**
     * @return all users.
     */
    List<User> selectAll();
    /**
     * @param role user role.
     * @return user list.
     */
    List<User> selectUser(Role role);
    /**
     * @param id user id.
     * @return user.
     */
    User selectUser(int id);

    /**
     * @param id user id.
     */
    void deleteUser(int id);

    boolean isExist(String email,
                    String password);

    void updateTotalSum(User user, Double cost);
}
