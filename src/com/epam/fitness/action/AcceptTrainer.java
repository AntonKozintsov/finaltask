package com.epam.fitness.action;

import com.epam.fitness.dao.mysql.SubscriptionDaoImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AcceptTrainer implements Action {
    @Override
    public String doAction(HttpServletRequest req, HttpServletResponse resp) {
        String page;
        HttpSession session = req.getSession(false);
        String message;
        String subscription_id = req.getParameter("subscription_id");
        String trainer_id = (String) req.getSession().getAttribute("trainer_id");
        System.out.println(trainer_id);
        SubscriptionDaoImpl subscriptionDao = new SubscriptionDaoImpl();
        if (trainer_id != null) {
            subscriptionDao.updateTrainerInfo(Integer.parseInt(subscription_id), Integer.parseInt(trainer_id));
            req.getSession().removeAttribute("trainer_id");
            message = "you have chosen a trainer";
        } else {
            message = "you need to choose a trainer";
        }
        session.setAttribute("message", message);
        page = "/user.jsp";
        return page;
    }
}
