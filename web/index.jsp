<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="website/css/style.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Fitness</title>
</head>
<body>
<div class="nav1">
    <nav class="navbar navbar-expand-md navbar-dark">
        <form method="post" action="fitness">
            <button type="submit" name="action" value="homepage" class="navbar-brand">
                Fitness
            </button>
        </form>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item acive">
                    <form method="post" action="fitness">
                        <button type="submit" name="action" value="homepage" class="nav-link navbutt">home</button>
                    </form>
                </li>
                <li class="nav-item">
                    <form method="post" action="fitness">
                        <button type="submit" name="action" value="aboutpage" class="nav-link navbutt">about</button>
                    </form>
                </li>
                <li class="nav-item">
                    <form method="post" action="fitness">
                        <button type="submit" name="action" value="newspage" class="nav-link navbutt">news</button>
                    </form>
                </li>
                <li class="nav-item">
                    <form method="post" action="fitness">
                        <button type="submit" name="action" value="contactpage" class="nav-link navbutt">contact
                        </button>
                    </form>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <form method="post" action="fitness">
                    <button type="submit" name="action" value="loginpage" class="nav-link" id="a1">LOGIN</button>
                </form>
            </ul>
        </div>
    </nav>
</div>

<div class="conteiner-fluid">
    <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleFade" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleFade" data-slide-to="1"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block" src="website/img/slider-1.jpg" alt="First slide">
                <div class="carousel-caption">
                    <h1 class="carouselName">Health is wealth</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi unde impedit, necessitatibus,
                        soluta sit quam minima expedita atque corrupti reiciendis.m.</p>
                </div>
            </div>
            <div class="carousel-item">
                <img class="d-block" src="website/img/slider-2.jpg" alt="Second slide">
                <div class="carousel-caption">
                    <h1 class="carouselName">Join fitness today</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi unde impedit, necessitatibus,
                        soluta sit quam minima expedita atque corrupti reiciendis.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="conteiner-fluid sectin2">
    <div class="row">
        <div class="col-md-12 text-center heading-wrap">
            <h2>Featured Classes</h2>
            <span class="back-text">The Classes</span>
        </div>
    </div>
</div>
<div class="conteiner-fluid novision">
    <div class="row">
        <div class="col-4 informat">
            <div class="text-center">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus et dolor blanditiis
                    consequuntur ex voluptates perspiciatis omnis unde minima expedita.</p>
            </div>
        </div>
        <div class="col-4 informat">
            <div class="text-center">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus et dolor blanditiis
                    consequuntur ex voluptates perspiciatis omnis unde minima expedita.</p>
            </div>
        </div>
        <div class="col-4 informat">
            <div class="text">
                <div class="text-center">
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus et dolor blanditiis
                        consequuntur ex voluptates perspiciatis omnis unde minima expedita.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="conteiner-fluid">
    <div class="conteiner3">
        <div class="row text-center">
            <div class="col-4 zoom"><img src="website/img/img_1_square.jpg" alt="#" class="w-100 getimg">
                <div class="text img1">
                    <p class="textbox">Fitness class name here</p>
                </div>
            </div>
            <div class="col-4 zoom"><img src="website/img/img_2_square.jpg" alt="#" class="w-100 getimg">
                <div class="text img1">
                    <p class="textbox">Fitness class name here</p>
                </div>
            </div>
            <div class="col-4 zoom"><img src="website/img/img_3_square.jpg" alt="#" class="w-100 getimg">
                <div class="text img1">
                    <p class="textbox">Fitness class name here</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="conteiner-fluid sectin2">
    <div class="row">
        <div class="col-md-12 text-center heading-wrap">
            <h2>Expert treiners</h2>
            <span class="back-text">Our Trainers</span>
        </div>
    </div>
</div>

<div class="conteiner-fluid">
    <div class="row text-center">
        <div class="col-sm-1"></div>
        <div class="col treiners"><img src="website/img/person_1.jpg" alt="#" class="w-100 sqr9">
        </div>
        <div class="col treiners"><img src="website/img/person_2.jpg" alt="#" class="w-100 sqr9">
        </div>
        <div class="col treiners"><img src="website/img/person_3.jpg" alt="#" class="w-100 sqr9">
        </div>
        <div class="col-sm-1"></div>
    </div>
</div>


<div class="conteiner-fluid">
    <div class="row text-center">
        <div class="col-sm-1"></div>
        <div class="col">
            <div class="text">
                <h2>Mellisa Howard</h2>
                <p class="text NameTr">Gym Trainer</p>
            </div>
        </div>
        <div class="col">
            <div class="text">
                <h2>Mike Richardson</h2>
                <p class="text NameTr">Gym Trainer</p>
            </div>
        </div>
        <div class="col">
            <div class="text">
                <h2>Charles White</h2>
                <p class="text NameTr">Gym Trainer</p>
            </div>
        </div>
        <div class="col-sm-1"></div>
    </div>
</div>


<footer class="page-footer">
    <div class="container-fluid text-center text-md-left">
        <div class="row">
            <div class="col-4"></div>
            <div class="col-2">
                <h3 class="text-white">About Us</h3>
                <p class="grey">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus et dolor
                    blanditiis consequuntur ex voluptates perspiciatis omnis unde minima expedita.</p>
            </div>
            <div class="col-2">
                <h3 class="text-white">Contact Info</h3>
                <ul class="list-unstyled">
                    <li class="d-block">
                        <span class="d-block grey">Address:</span>
                        <span class="text-white">34 Street Name, City Name Here, United States</span></li>
                    <li class="d-block"><span class="d-block grey">Telephone:</span><span class="text-white">+1 242 4942 290</span>
                    </li>
                    <li class="d-block"><span class="d-block grey">Email:</span><span class="text-white">info@yourdomain.com</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
</body>
</html>