package com.epam.fitness.action;

import com.epam.fitness.dao.mysql.UserDaoImpl;
import com.epam.fitness.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Class to deposit money.
 */
public class UpBalance implements Action {
    /**
     * Log4j.
     */
    private static final Logger LOG_ERROR = LogManager.getLogger("ERROR");

    /**
     * {@inheritDoc}
     */
    @Override
    public String doAction(final HttpServletRequest req,
                           final HttpServletResponse resp)
            throws IOException {
        String page;
        UserDaoImpl userDao = new UserDaoImpl();
        User user = (User) req.getSession().getAttribute("user");
        if (req.getParameter("money").isEmpty()) {
            LOG_ERROR.error(user.toString() + "desosit failed");
        } else {
            double money = Double.valueOf(req.getParameter("money"));
            user.setMoney(user.getMoney() + money);
            userDao.updateMoney(user.getEmail(), user.getMoney());
            req.getSession().setAttribute("user", user);
        }
        page = "/user.jsp";
        return page;
    }
}
