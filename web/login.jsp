<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="website/css/member.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script type="text/javascript">
        <%@include file="/website/js/scripts.js"%>
    </script>
    <script src='https://unpkg.com/sweetalert/dist/sweetalert.min.js'></script>
    <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js'></script>
    <title>Fitness</title>
</head>
<body>
<div class="nav1">
    <nav class="navbar navbar-expand-md navbar-dark">
        <form method="post" action="fitness">
            <button type="submit" name="action" value="homepage" class="navbar-brand">
                Fitness
            </button>
        </form>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item acive">
                    <form method="post" action="fitness">
                        <button type="submit" name="action" value="homepage" class="nav-link">home</button>
                    </form>
                </li>
                <li class="nav-item">
                    <form method="post" action="fitness">
                        <button type="submit" name="action" value="aboutpage" class="nav-link">about
                        </button>
                    </form>
                </li>
                <li class="nav-item">
                    <form method="post" action="fitness">
                        <button type="submit" name="action" value="newspage" class="nav-link">news</button>
                    </form>
                </li>
                <li class="nav-item">
                    <form method="post" action="fitness">
                        <button type="submit" name="action" value="contactpage" class="nav-link">contact
                        </button>
                    </form>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <form method="post" action="fitness">
                    <button type="submit" name="action" value="loginpage" class="nav-link" id="a1">LOGIN</button>
                </form>
            </ul>
        </div>
    </nav>
</div>
<div class="conteiner-fluid">
    <div class="conteiner1">
        <div class="row text-center">
            <img src="website/img/slider-1.jpg" class="w-100 bigimg">
            <div class="text"><h1 class="about">Log in</h1></div>
        </div>
    </div>
</div>
<div class="conteiner-fluid">
    <div class="row">
        <div class="col-4"></div>
        <div class="col-4 formreg">
            <form method="post" action="fitness">
                <div class="text logtext">LOG IN</div>
                <div class="form-group">
                    <input type="email" name="email" class="form-control control1" id="exampleInputEmail1"
                           aria-describedby="emailHelp" placeholder="Email">
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control control2" id="exampleInputPassword1"
                           placeholder="Password">
                </div>
                <button type="submit" name="action" value="login" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
<c:if test="${sessionScope.message != null}">
    <script>
        var loginMessage = "${sessionScope.message}";
        actionAlert(loginMessage);
    </script>
    ${sessionScope.message=null};
</c:if>
<footer class="page-footer">
    <div class="container-fluid text-center text-md-left">
        <div class="row">
            <div class="col-4"></div>
            <div class="col-2">
                <h3 class="text-white">About Us</h3>
                <p class="grey">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus et dolor
                    blanditiis consequuntur ex voluptates perspiciatis omnis unde minima expedita.</p>
            </div>
            <div class="col-2">
                <h3 class="text-white">Contact Info</h3>
                <ul class="list-unstyled footer-link">
                    <li class="d-block">
                        <span class="d-block grey">Address:</span>
                        <span class="text-white">34 Street Name, City Name Here, United States</span></li>
                    <li class="d-block"><span class="d-block grey">Telephone:</span><span class="text-white">+1 242 4942 290</span>
                    </li>
                    <li class="d-block"><span class="d-block grey">Email:</span><span class="text-white">info@yourdomain.com</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
</body>
</html>