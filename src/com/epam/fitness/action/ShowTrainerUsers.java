package com.epam.fitness.action;

import com.epam.fitness.dao.mysql.UserDaoImpl;
import com.epam.fitness.entity.Role;
import com.epam.fitness.entity.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ShowTrainerUsers implements Action {
    @Override
    public String doAction(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String page;
        UserDaoImpl userDao = new UserDaoImpl();
        List<User> userList;
        userList = userDao.selectUser(Role.USER);
        page = "/trainer.jsp";
        req.getSession().setAttribute("users", userList);
        return page;
    }
}
