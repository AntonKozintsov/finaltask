package com.epam.fitness.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Abstract class for dao.
 */
public abstract class AbstractDao {
    /**
     * Method to close statement.
     * @param statement statement.
     */
    protected void closeConnection(final Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    /**
     * Method to close connection.
     * @param connection connection.
     */
    protected void closeStatement(final Connection connection) {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
