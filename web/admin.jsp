<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <%--<link rel="stylesheet" type="text/css" href="website/css/style.css">--%>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/website/css/style.css"/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Fitness</title>
</head>
<body>
<div class="userInf">
    <c:set var="user" value="${user}"/>
    <div class="text userName"><c:out value="${user.getName()}"/></div>
    <form method="post" action="fitness">
        <button name="action" value="logout" class="btn btn-primary logout">LOGOUT</button>
    </form>

</div>

<div class="conteiner-fluid sectin2">
    <div class="row">
        <div class="col-md-12 text-center heading-wrap">
            <h2>Administrator Options</h2>
        </div>
    </div>
</div>
<div>
    <form method="post" action="fitness">
        <button class="btn btn-primary logout" name="action" value="users"><c:out value="SHOW USERS"/></button>
        <button class="btn btn-primary logout" name="action" value="trainers"><c:out value="SHOW TRAINERS"/></button>
    </form>
</div>

<footer class="page-footer">
    <div class="container-fluid text-center text-md-left">
        <div class="row">
            <div class="col-4"></div>
            <div class="col-2">
                <h3 class="text-white">About Us</h3>
                <p class="grey">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus et dolor
                    blanditiis consequuntur ex voluptates perspiciatis omnis unde minima expedita.</p>
            </div>
            <div class="col-2">
                <h3 class="text-white">Contact Info</h3>
                <ul class="list-unstyled">
                    <li class="d-block">
                        <span class="d-block grey">Address:</span>
                        <span class="text-white">34 Street Name, City Name Here, United States</span></li>
                    <li class="d-block"><span class="d-block grey">Telephone:</span><span class="text-white">+1 242 4942 290</span>
                    </li>
                    <li class="d-block"><span class="d-block grey">Email:</span><span class="text-white">info@yourdomain.com</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
</body>
</html>