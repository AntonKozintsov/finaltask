package com.epam.fitness.entity;

/**
 * User.
 */
public class User extends Entity {
    /**
     * Password.
     */
    private String password;
    /**
     * Role.
     */
    private Role role;
    /**
     * Name.
     */
    private String name;
    /**
     * Email.
     */
    private String email;
    /**
     * Money.
     */
    private double money;

    private double totalSum;

    /**
     * Default constructor.
     */
    public User() {

    }

    /**
     * {@inheritDoc}
     */
    public double getMoney() {
        return money;
    }

    /**
     * {@inheritDoc}
     */
    public void setMoney(final double money) {
        this.money = money;
    }

    /**
     * {@inheritDoc}
     */
    public String getPassword() {
        return password;
    }

    /**
     * {@inheritDoc}
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * {@inheritDoc}
     */
    public Role getRole() {
        return role;
    }

    /**
     * {@inheritDoc}
     */
    public void setRole(final Role role) {
        this.role = role;
    }

    /**
     * {@inheritDoc}
     */
    public String getName() {
        return name;
    }

    /**
     * {@inheritDoc}
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * {@inheritDoc}
     */
    public String getEmail() {
        return email;
    }

    /**
     * {@inheritDoc}
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    public double getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(double totalSum) {
        this.totalSum = totalSum;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "User{" + "password='" + password
                + '\'' + ", role=" + role + ", name='"
                + name + '\'' + ", email='" + email
                + '\'' + ", money=" + money + '}';
    }
}
