package com.epam.fitness.dao.mysql;

import com.epam.fitness.dao.OrderDao;
import com.epam.fitness.dao.pool.ConnectionPool;
import com.epam.fitness.entity.Order;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Order dao implementation.
 */
public class OrderDaoImpl extends BaseDaoImpl implements OrderDao {
    /**
     * {@inheritDoc}
     */
    @Override
    public void insert(final Order order) {
        String sql = "INSERT INTO `order` "
                + "(`user_id`, `subscription_id`, `date`, `cost`) VALUES(?, ?, ?, ?)";
        PreparedStatement statement = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            statement = connection.prepareStatement(sql);

            statement.setInt(1,
                    order.getUser().getIdentity());
            statement.setInt(2, order.getSubscription().getIdentity());
            statement.setDate(3, order.getDate());
            statement.setBigDecimal(4,
                    new BigDecimal(order.getCost()));
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            super.closeConnection(connection);
            super.closeStatement(statement);
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public List<Order> selectOrder(final int userId) {
        List<Order> orderList = new ArrayList<>();
        String sql = "SELECT * FROM `order` o WHERE o.`user_id` = ?";
        ResultSet resultSet = null;
        PreparedStatement statement = null;
        Order order;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            statement = connection.prepareStatement(sql);
            statement.setInt(1, userId);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                order = create(resultSet);
                orderList.add(order);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            super.closeConnection(connection);
            super.closeStatement(statement);
        }
        return orderList;
    }

    /**
     * Method to create user.
     * @param resultSet resultSet.
     * @return user.
     */
    private Order create(final ResultSet resultSet) {
        Order order = new Order();
        UserDaoImpl userDao = new UserDaoImpl();
        SubscriptionDaoImpl subscriptionDao = new SubscriptionDaoImpl();
        if (resultSet != null) {
            try {
                order.setSubscription(subscriptionDao.selectSubscription(resultSet.getInt("subscription_id")));
                order.setIdentity(resultSet.getInt("id"));
                order.setUser(userDao.selectUser(resultSet.getInt("user_id")));
                order.setCost(resultSet.getDouble("cost"));
                order.setDate(resultSet.getDate("date"));
                order.setExpiration(new java.sql.Date(new java.util.Date()
                        .getTime() + 30L * 24 * 60 * 60 * 1000));
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return order;
    }
}
