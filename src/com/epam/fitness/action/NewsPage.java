package com.epam.fitness.action;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class NewsPage implements Action {
    @Override
    public String doAction(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        return "/news.jsp";
    }
}
