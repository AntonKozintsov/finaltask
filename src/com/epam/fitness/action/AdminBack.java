package com.epam.fitness.action;

import com.epam.fitness.entity.Role;
import com.epam.fitness.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AdminBack implements Action {
    @Override
    public String doAction(HttpServletRequest req, HttpServletResponse resp) {
        String page;
        User user  = (User) req.getSession().getAttribute("user");
        if (user.getRole().equals(Role.ADMINISTRATOR)) {
            page = "/admin.jsp";
        } else {
            page = "/trainer.jsp";
        }
        return page;
    }
}
