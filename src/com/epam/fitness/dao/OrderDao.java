package com.epam.fitness.dao;

import com.epam.fitness.entity.Order;

import java.util.List;

/**
 * Interface for order dao.
 */
public interface OrderDao {
    /**
     * @param id user id,
     * @return list of user orders.
     */
    List<Order> selectOrder(int id);

    /**
     * @param order order.
     */
    void insert(Order order);
}
