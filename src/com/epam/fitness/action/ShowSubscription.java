package com.epam.fitness.action;

import com.epam.fitness.dao.OrderDao;
import com.epam.fitness.dao.mysql.OrderDaoImpl;
import com.epam.fitness.entity.Order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Class to view user subscriptions.
 */
public class ShowSubscription implements Action {
    /**
     * {@inheritDoc}
     */
    @Override
    public String doAction(final HttpServletRequest req,
                           final HttpServletResponse resp) {
        String page;
        String message;
        int id = Integer.parseInt(req.getParameter("user_id"));
        OrderDao orderDao;
        List<Order> orderList;
        HttpSession session = req.getSession(false);
        orderDao = new OrderDaoImpl();
        orderList = orderDao.selectOrder(id);
        String trainer_id = req.getParameter("trainer_id");
        if (trainer_id != null) {
            message = "choose subscription to accept trainer";
            session.setAttribute("orders", orderList);
        } else if (orderList.isEmpty()) {
            message = "your subscription list is empty";
        } else {
            message = "your subscription list is ready";
            session.setAttribute("orders", orderList);
        }
        session.setAttribute("message", message);
        page = "/user.jsp";
        return page;
    }

}
