package com.epam.fitness.action;

import com.epam.fitness.dao.mysql.OrderDaoImpl;
import com.epam.fitness.dao.mysql.SubscriptionDaoImpl;
import com.epam.fitness.dao.mysql.UserDaoImpl;
import com.epam.fitness.entity.Order;
import com.epam.fitness.entity.Subscription;
import com.epam.fitness.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;

/**
 * Class to buy subscription.
 */
public class MakeOrder implements Action {
    /**
     * Log4j.
     */
    private static final Logger LOG =
            LogManager.getLogger(LogOut.class.getName());

    /**
     * {@inheritDoc}
     */
    @Override
    public String doAction(final HttpServletRequest req,
                           final HttpServletResponse resp)
            throws IOException {
        String page;
        double cost = Double.parseDouble(req.getParameter("cost"));
        HttpSession session = req.getSession(false);
        String message;
        UserDaoImpl userDao = new UserDaoImpl();
        User user = userDao.selectUser(String.valueOf(req.getSession().getAttribute("email")));

        double newTotalSum = user.getTotalSum() + setFinalCost(user.getTotalSum(),cost);
        OrderDaoImpl orderDao = new OrderDaoImpl();
        SubscriptionDaoImpl subscriptionDao = new SubscriptionDaoImpl();
        Subscription subscription;
        Order order;
        if (user.getMoney() < setFinalCost(newTotalSum, cost)) {
            message = "failed to purchase subscription";
            LOG.info(user.toString() + message);
        } else {
            User trainer = new User();
            subscription = new Subscription();
            subscription.setVisitCount(setVisitCount(cost));
            subscription.setUser(user);
            subscription.setTrainer(trainer);
            subscription.setInfo("Pick trainer to get info");
            subscriptionDao.insert(subscription);
            order = new Order();
            order.setCost(setFinalCost(newTotalSum, cost));
            order.setUser(user);
            order.setSubscription(subscriptionDao.selectSubscriptionDesc(user.getIdentity()));
            Date date = new Date();
            date.getTime();
            order.setDate(new java.sql.Date(new Date().getTime()));
            orderDao.insert(order);
            user.setMoney(user.getMoney() - setFinalCost(newTotalSum, cost));
            userDao.updateMoney(user.getEmail(), user.getMoney());
            userDao.updateTotalSum(user, newTotalSum);
            req.getSession().setAttribute("user", userDao.selectUser(String.valueOf(req.getSession().getAttribute("email"))));
            message = "subscription purchased successfully";
//            SendEmail.send(user.getEmail(), message, order.toString());
            LOG.info(user.toString() + message);
        }
        page = "/user.jsp";
        session.setAttribute("message", message);
        return page;
    }

    private int setVisitCount(double cost) {
        int visitCount = 0;
        switch ((int) cost) {
            case 20:
                visitCount = 8;
                break;
            case 25:
                visitCount = 12;
                break;
            case 30:
                visitCount = 15;
                break;

        }
        return visitCount;
    }

    private double setFinalCost(double total_sum, double cost) {
        if (total_sum > 200) {
            return cost - 3;
        } else if (total_sum > 500) {
            return cost - 5;
        } else {
            return cost;
        }
    }

}
