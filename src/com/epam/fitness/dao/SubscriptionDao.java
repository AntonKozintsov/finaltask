package com.epam.fitness.dao;

import com.epam.fitness.entity.Subscription;

import java.util.List;

public interface SubscriptionDao {
    /**
     * @param id user id,
     * @return list of user subscriptions.
     */
    List<Subscription> selectSubscriptionList(int id);

    /**
     * @param subscription subscription.
     */
    void insert(Subscription subscription);
    Subscription selectSubscription(int id);
    Subscription selectSubscriptionDesc(int id);
    void updateTrainerInfo(int subscription_id, int trainer_id);
}
