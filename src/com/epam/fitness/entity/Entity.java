package com.epam.fitness.entity;

/**
 * Abstract entity.
 */
public abstract class Entity {
    /**
     * Id.
     */
    private Integer identity;

    /**
     * {@inheritDoc}
     */
    public Integer getIdentity() {
        return identity;
    }

    /**
     * {@inheritDoc}
     */
    public void setIdentity(final Integer identity) {
        this.identity = identity;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object object) {
        if (object != null) {
            if (object != this) {
                if (object.getClass() == getClass() && identity != null) {
                    return identity.equals(((Entity) object).identity);
                }
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return identity != null ? identity.hashCode() : 0;
    }
}
