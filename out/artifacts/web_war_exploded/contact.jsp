<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="website/css/contact.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>Fitness</title>
</head>
<body>
<div class="conteiner-fluid">
    <div class="conteiner1">
        <div class="row">
            <img src="website/img/slider-1.jpg" class="w-100 bigimg">
            <div class="text"><h1 class="about">Contact</h1></div>
        </div>
    </div>
</div>
<div class="nav1">
    <nav class="navbar navbar-expand-md navbar-dark">
        <form method="post" action="fitness">
            <button type="submit" name="action" value="homepage" class="navbar-brand">
                Fitness
            </button>
        </form>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item acive">
                    <form method="post" action="fitness">
                        <button type="submit" name="action" value="homepage" class="nav-link navbutt">home</button>
                    </form>
                </li>
                <li class="nav-item">
                    <form method="post" action="fitness">
                        <button type="submit" name="action" value="aboutpage" class="nav-link navbutt">about
                        </button>
                    </form>
                </li>
                <li class="nav-item">
                    <form method="post" action="fitness">
                        <button type="submit" name="action" value="newspage" class="nav-link navbutt">news</button>
                    </form>
                </li>
                <li class="nav-item">
                    <form method="post" action="fitness">
                        <button type="submit" name="action" value="contactpage" class="nav-link navbutt">contact
                        </button>
                    </form>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <form method="post" action="fitness">
                    <button type="submit" name="action" value="loginpage" class="nav-link" id="a1">LOGIN</button>
                </form>
            </ul>
        </div>
    </nav>
</div>
<div class="conteiner-fluid sectin2">
    <div class="row">
        <div class="col-md-12 text-center heading-wrap">
            <h2>Get in touch</h2>
            <span class="back-text">Contact</span>
        </div>
    </div>
</div>

<div class="conteiner-fluid">
    <div class="row">
        <div class="col-4"></div>
        <div class="col-4 shad">
            <div class="text">
                <h1>Contact Details</h1>
                    <p class="p1">34 Street Name, City Name Here, United States</p>
                    <p class="p1">+1 242 4942 290</p>
                    <p class="p1">info@yourdomain.com</p>
            </div>
            <div class="col-4"></div>
        </div>
    </div>

    <footer class="page-footer">
        <div class="container-fluid text-center text-md-left">
            <div class="row">
                <div class="col-4"></div>
                <div class="col-2">
                    <h3 class="text-white">About Us</h3>
                    <p class="grey">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus et dolor
                        blanditiis consequuntur ex voluptates perspiciatis omnis unde minima expedita.</p>
                </div>
                <div class="col-2">
                    <h3 class="text-white">Contact Info</h3>
                    <ul class="list-unstyled footer-link">
                        <li class="d-block">
                            <span class="d-block grey">Address:</span>
                            <span class="text-white">34 Street Name, City Name Here, United States</span></li>
                        <li class="d-block"><span class="d-block grey">Telephone:</span><span class="text-white">+1 242 4942 290</span>
                        </li>
                        <li class="d-block"><span class="d-block grey">Email:</span><span class="text-white">info@yourdomain.com</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
            integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>
</body>
</html>