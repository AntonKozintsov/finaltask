package com.epam.fitness.action;

import com.epam.fitness.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Class to log out.
 */
public class LogOut implements Action {
    /**
     * Log4j.
     */
    private static final Logger LOG =
            LogManager.getLogger(LogOut.class.getName());

    /**
     * {@inheritDoc}
     */
    @Override
    public String doAction(final HttpServletRequest req,
                           final HttpServletResponse resp) throws IOException, ServletException {
        User user = (User) req.getSession().getAttribute("user");
        LOG.info(user.toString() + "user logged out");
        String page;
        req.getSession().removeAttribute("user");
        req.getSession().invalidate();
        page = "/index.jsp";
        return page;
    }
}
