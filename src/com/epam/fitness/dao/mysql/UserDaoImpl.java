package com.epam.fitness.dao.mysql;

import com.epam.fitness.dao.UserDao;
import com.epam.fitness.dao.pool.ConnectionPool;
import com.epam.fitness.entity.Role;
import com.epam.fitness.entity.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * User dao implementation.
 */
public class UserDaoImpl extends BaseDaoImpl implements UserDao {


    /**
     * {@inheritDoc}
     */
    @Override
    public User selectUser(final String email) {
        String sql = "SELECT * FROM `user` WHERE email = ?";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        User user = new User();
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, email);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                user = create(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
        super.closeConnection(connection);
        super.closeStatement(preparedStatement);
    }
        return user;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public User selectUser(final int id) {
        String sql = "SELECT * FROM `user` u WHERE u.`id` = ?";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        User user = new User();
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = create(resultSet);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            super.closeConnection(connection);
            super.closeStatement(preparedStatement);
        }
        return user;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateMoney(final String email, final Double money) {
        String sql = "UPDATE `user` u SET u.`money` "
                + "= ? WHERE u.`email` = ?";
        PreparedStatement preparedStatement = null;
        User user = this.selectUser(email);
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setDouble(1, money);
            preparedStatement.setString(2, user.getEmail());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            super.closeConnection(connection);
            super.closeStatement(preparedStatement);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<User> selectAll() {
        String sql = "SELECT * FROM `user`";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        List<User> userList = new ArrayList<>();
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(
                    sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                userList.add(create(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            super.closeConnection(connection);
            super.closeStatement(preparedStatement);
        }
        return userList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<User> selectUser(final Role role) {
        String sql = "SELECT * FROM `user` u WHERE u.`role` = ?";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet;
        List<User> userList = new ArrayList<>();
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            switch (role) {
                case USER:
                    preparedStatement.setInt(1, 0);
                    break;
                case TRAINER:
                    preparedStatement.setInt(1, 1);
                    break;
                default:
                    break;
            }
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                userList.add(create(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            super.closeConnection(connection);
            super.closeStatement(preparedStatement);
        }
        return userList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteUser(final int id) {
        String sql = "DELETE FROM `user` WHERE `id` = ?";
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            super.closeStatement(preparedStatement);
            super.closeConnection(connection);
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isExist(final String email,
                           final String password) {
        String sql = "SELECT * FROM `user` WHERE `email` = ? "
                + "AND `password` = ?";
        PreparedStatement preparedStatement = null;
        final ResultSet resultSet;
        boolean result = false;
        try {

            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(
                    sql);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                result = true;
            }


        } catch (SQLException e) {

            e.printStackTrace();


        } finally {
            super.closeConnection(connection);
            super.closeStatement(preparedStatement);
        }

        return result;

    }
    @Override
    public void updateTotalSum(final User user, final Double newTotalSum) {
        String sql = "UPDATE `user` u SET u.`totalsum` "
                + "= ? WHERE u.`email` = ?";
        PreparedStatement preparedStatement = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setDouble(1, newTotalSum);
            preparedStatement.setString(2, user.getEmail());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            super.closeConnection(connection);
            super.closeStatement(preparedStatement);
        }
    }
    /**
     * Method to create user.
     *
     * @param resultSet resultSet.
     * @return user.
     */
    private static User create(final ResultSet resultSet) {
        User user = new User();

        if (resultSet != null) {
            try {

                user.setIdentity(resultSet.getInt("id"));
                user.setPassword(resultSet.getString("password"));
                user.setName(resultSet.getString("name"));
                user.setEmail(resultSet.getString("email"));
                user.setMoney(resultSet.getDouble("money"));
                user.setTotalSum(resultSet.getInt("totalsum"));
                switch (resultSet.getString("role")) {
                    case "0":
                        user.setRole(Role.USER);
                        break;
                    case "1":
                        user.setRole(Role.TRAINER);
                        break;
                    case "2":
                        user.setRole(Role.ADMINISTRATOR);
                        break;
                    default:
                        break;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return user;
    }
}
