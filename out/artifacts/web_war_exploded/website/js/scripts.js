function submitForm(x){
    switch (x.id) {
        case "ord1":
            document.getElementById('cost').value='20';
            break;
        case "ord2":
            document.getElementById('cost').value='25';
            break;
        case "ord3":
            document.getElementById('cost').value='30';
            break;
        case "trainer1":
            document.getElementById('trainer_id').value='1';
            break;
        case "trainer2":
            document.getElementById('trainer_id').value='2';
            break;
        case "trainer3":
            document.getElementById('trainer_id').value='3';
            break;

    }
    document.forms[0].submit();
}



function actionAlert(message) {
    switch (message) {
        case "subscription purchased successfully" :
            swal("Great!", message, "success");
            break;
        case "failed to purchase subscription":
            swal ( "Oops" , message ,  "error" );
            break;
        case "your subscription list is ready" :
            swal (message);
            break;
        case "your subscription list is empty":
            swal (message);
            break;
        case "wrong login or password" :
            swal ("Error while logging", message, "error");
            break;
        case "you successfully logged in":
            swal("Great!", message, "success");
            break;
        case "you need to choose a trainer" :
            swal ("Error, accept trainer first", message, "error");
            break;
        case "you have chosen a trainer":
            swal("Enjoy training info!", message, "success");
            break;
        case "choose subscription to accept trainer":
            swal(message);
    }
}
