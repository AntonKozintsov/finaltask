package com.epam.fitness.entity;

import java.sql.Date;

/**
 * Order.
 */
public class Order extends Entity {
    /**
     * User.
     */
    private User user;
    /**
     * Order cost.
     */
    private Subscription subscription;

    private Double cost;
    /**
     * Order date.
     */
    private Date date;
    /**
     * Order expiration.
     */
    private Date expiration;

    /**
     * default constructor.
     */
    public Order() {
    }

    /**
     * {@inheritDoc}
     */
    public Date getDate() {
        return date;
    }

    /**
     * {@inheritDoc}
     */
    public void setDate(final Date date) {
        this.date = date;
    }

    /**
     * {@inheritDoc}
     */
    public User getUser() {
        return user;
    }

    /**
     * {@inheritDoc}
     */
    public void setUser(final User user) {
        this.user = user;
    }

    /**
     * {@inheritDoc}
     */
    public Double getCost() {
        return cost;
    }

    /**
     * {@inheritDoc}
     */
    public void setCost(final Double cost) {
        this.cost = cost;
    }

    /**
     * {@inheritDoc}
     */
    public Date getExpiration() {
        return expiration;
    }

    /**
     * {@inheritDoc}
     */
    public void setExpiration(final Date expiration) {
        this.expiration = expiration;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Order{" +
                "user=" + user +
                ", subscription=" + subscription +
                ", cost=" + cost +
                ", date=" + date +
                ", expiration=" + expiration +
                '}';
    }
}
