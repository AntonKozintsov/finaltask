package com.epam.fitness.util;

import com.epam.fitness.entity.Role;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class to redirect user.
 */
public final class UserRedirect {
    /**
     * private constructor.
     */
    private UserRedirect() {
    }

    /**
     * @param req  request.
     * @param resp response.
     * @param role role.
     */
    public static String redirect(final HttpServletRequest req,
                                final HttpServletResponse resp,
                                final Role role) {
        String redirect = null;
        switch (role) {

            case USER:
                redirect = ("/user.jsp");
                break;
            case ADMINISTRATOR:
                redirect = ("/admin.jsp");
                break;
            case TRAINER:
                redirect = ("/trainer.jsp");
                break;
            default:
                break;
        }
        return redirect;
    }
}
