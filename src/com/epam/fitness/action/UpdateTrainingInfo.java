package com.epam.fitness.action;

import com.epam.fitness.dao.mysql.SubscriptionDaoImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UpdateTrainingInfo implements Action {
    @Override
    public String doAction(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String training_info = req.getParameter("training_info");
        int subscription_Id = Integer.parseInt(req.getParameter("subscription_id"));
        SubscriptionDaoImpl subscriptionDao = new SubscriptionDaoImpl();
        subscriptionDao.updateInfo(training_info, subscription_Id);
        System.out.println(training_info);
        return "/trainer.jsp";
    }
}
