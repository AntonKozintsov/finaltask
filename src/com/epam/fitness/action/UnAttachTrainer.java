package com.epam.fitness.action;

import com.epam.fitness.dao.mysql.SubscriptionDaoImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UnAttachTrainer implements Action {
    @Override
    public String doAction(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String page;
        SubscriptionDaoImpl subscriptionDao = new SubscriptionDaoImpl();
        int subscription_Id = Integer.parseInt(req.getParameter("subscription_id"));
        subscriptionDao.updateTrainer(subscription_Id);
        page="/users.jsp";
        return page;
    }
}
