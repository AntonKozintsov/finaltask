package com.epam.fitness.entity;

public class Subscription extends Entity {
    private int visitCount;
    private User user;
    private User trainer;
    private String info;

    public int getVisitCount() {
        return visitCount;
    }

    public void setVisitCount(int visitCount) {
        this.visitCount = visitCount;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getTrainer() {
        return trainer;
    }

    public void setTrainer(User trainer) {
        this.trainer = trainer;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "Subscription{" +
                "visitCount=" + visitCount +
                ", user=" + user +
                ", trainer=" + trainer +
                ", info='" + info + '\'' +
                '}';
    }
}
