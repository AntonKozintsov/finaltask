package com.epam.fitness.util;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Class to send email.
 */
public final class SendEmail {
    /**
     * private constructor.
     */
    private SendEmail() {
    }
    /**
     * @param userMail user email.
     * @param orderMessage message to send.
     * @param order order.
     */
    public static void send(final String userMail,
                            final String orderMessage, final String order) {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("bleac96@gmail.com", "batya322");
                    }
                });
        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("bleac96@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(userMail));
            message.setSubject("Purchasing subscription");
            message.setText(orderMessage + " " + order);
            Transport.send(message);

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}
