package com.epam.fitness.action;

import com.epam.fitness.dao.mysql.UserDaoImpl;
import com.epam.fitness.entity.User;
import com.epam.fitness.util.HashPassword;
import com.epam.fitness.util.UserRedirect;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Class for user authorization.
 */
public class Authorization implements Action {
    /**
     * Log4j.
     */
    private static final Logger LOG =
            LogManager.getLogger(Authorization.class.getName());

    /**
     * {@inheritDoc}
     */
    @Override
    public String doAction(final HttpServletRequest req,
                           final HttpServletResponse resp) {
        String page;
        String message;
        String email = req.getParameter("email");
        String password = (req.getParameter("password"));
        HttpSession session = req.getSession(false);
        UserDaoImpl userDao = new UserDaoImpl();
        User user;
        if (userDao.isExist(email, HashPassword.generateHashMD5(password))) {
            user = userDao.selectUser(email);
            session.setAttribute("user", user);
            session.setAttribute("email", email);
            message = "you successfully logged in";
            LOG.info(user.toString() + message);
            page = UserRedirect.redirect(req, resp, user.getRole());
        } else {
            message = "wrong login or password";
            page = "/login.jsp";
            LOG.info(message);
        }
        session.setAttribute("message", message);
        return page;
    }


}
