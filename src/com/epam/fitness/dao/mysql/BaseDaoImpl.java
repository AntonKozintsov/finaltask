package com.epam.fitness.dao.mysql;

import com.epam.fitness.dao.pool.ConnectionPool;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

abstract public class BaseDaoImpl {
    protected Connection connection;

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    protected void closeStatement(final Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Method to close connection.
     *
     * @param connection connection.
     */
    protected void  closeConnection(final Connection connection) {
        if (connection != null) {
            ConnectionPool.getInstance().freeConnection(connection);
        }
    }
}
