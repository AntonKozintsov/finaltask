package com.epam.fitness.action;

import com.epam.fitness.dao.mysql.UserDaoImpl;
import com.epam.fitness.entity.Role;
import com.epam.fitness.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Class to view user list.
 */
public class ShowUsers implements Action {
    /**
     * {@inheritDoc}
     */
    @Override
    public String doAction(final HttpServletRequest req,
                           final HttpServletResponse resp)
            throws IOException {
        String page;
        UserDaoImpl userDao = new UserDaoImpl();
        List<User> userList;
        userList = userDao.selectUser(Role.USER);
        page = "/users.jsp";
        req.getSession().setAttribute("users", userList);
        return page;
    }

}
